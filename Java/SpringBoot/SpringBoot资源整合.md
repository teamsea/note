# Spring Boot 资源整合

Spring 框架是一个资源整合框架,Spring Boot 对Spring 框架进行了优化

使用Spring Boot 整合资源十分的方便

## 1 Spring 整合数据库连接池

### 1.1 数据库连接池

实际开发中应用程序与数据库交互时，“获得连接”或“释放资源”是非常消耗系统资源的两个过程，为了解决此类性能问题，通常情况我们采用连接池技术来重用连接Connection对象

数据库连接池会创建并管理多个`Connection`对象,当我们需要Connection对象时,我们可以在连接池中就进行获取,不用手动进行创建和自行管理了

数据库连接池,再创建连接对象时,使用的亨源模式开发模式来创建大的连接对象

![](https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/1677660106813image14.png)

Java为数据库连接池提供了公共的接口：`javax.sql.DataSource`

各个厂商需要让自己的连接池实现这个接口。然后我们的应用程序中耦合与这个接口，便可以方便的切换不同厂商的连接池，常见的连接池有DBCP、C3P0，DRUID,HikariCP等。通过连接池获取连接的一个基本过程

![](https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/1677660169812image15.png)

上图(时序图)解析

1. User使用DataSource接口实现类的`getConnection()`方法来获取连接对象
2. 连接池对象来池中寻找连接对象
3. 如果连接池中没有连接对象,则调用Driver实现类的`connect()`方法
4. 根据数据库提供的启动程序,通过TCP/IP协议来获取到连接对象
5. 将创建的连接对象给Driver实现类
6. 将连接对象返还给连接池
7. 将获取的连接诶对象放到连接池中
8. 用户获取到连接对象

### 1.2 Spring Boot 获取和使用连接池

创建一个Spring项目,并且选择要使用的依赖

<img src="https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/16776663548121677666354090.png" style="zoom:67%;" />

选择以上依赖

spring boot 框架配置了一个连接池`HiKariCP`

HiKariCP,是现在最强的数据库连接池,也是spring boot 框架默认使用的数据库连接池

spring boot 框架只需要配置`src/main/recourse`中的`application.properties`文件就可以了

```properties
spring.datasource.url=jdbc:mysql://127.0.0.1:3306/user?serverTimezone=GMT%2B8
spring.datasource.username=root
spring.datasource.password=liliumx2
```

在url中可进行简写

```properties
spring.datasource.url=jdbc:mysql:///user?serverTimezone=GMT%2B8
```

地址和端口使用默认值,可以不写,所以有三个`/`

#### 1.2.1 使用HiKariCP

```java
@SpringBootTest
public class SpringBootDataSourceTest {

	
	@Autowired
	DataSource dataSource;
	
	@Test
	public void getDataSource() {
		System.out.println(dataSource);
	}
	
}
```

输出结果

```
HikariDataSource (null)
```

已经使用HikariCP数据库连接池 

#### 1.2.2 原理分析

![](https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/1677735615813image10.png)

1. 进入测试类,Spring Boot 框架开始运行
2. 对`DataSource`对象进行依赖注入
3. 因为`HiKariCPDataSource`接口实现的DataSource接口,所以会进行依赖注入
4. HiKariCP会通过`java.sql.Driver`接口的实例化对象来创建连接(不是数据库连接管理器)

### 1.3 Spring Boot 整合MyBatis框架

MyBatis是一个优秀的持久层框架,是对JDBC的进一步封装和使用,具有灵活的SQL定,有参数和结果集的映射方式更好的适应了互联网技术的发展

![](https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/1677746782806image18.png)

#### 环境配置

在`src/main/resource`配置文件`application.properties`对MyBatis框架进行简单配置

```properties
mybatis.configuration.default-statement-timeout=30
mybatis.configuration.map-underscore-to-camel-case=true
```

注意,MyBatis框架可以不用配置直接进行使用


配置mybatis中的sql日志的输出：(com.teamsea为我们写的项目的根包)

```properties
logging.level.com.teamsea=DEBUG
```

#### 业务实现

![](https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/1677758573688image8.png)

1. Spring Boot 测试类开始工作,Spring 框架给Mapper接口注入依赖
2. 这个Mapper实例是MyBatis框架产生的一个实现该Mapper接口的代理类

#### 项目测试

创建数据表`users`

```sql
-- `user`.users definition

CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
```

创建Bean组件

```java
@Component
public class User {
	private String name;
	public User() {}
    // getter and setter
}
```

创建Mapper接口

Mapper接口类要被`@Mapper`修饰

+ `@Mapper`注解用于描述数据层接口,由MyBatis框架定义
+ MyBatis框架会对这个接口类进行实例化对象,使用的方法是JDK中的代理工具类
+ 将实例化对象交个Spring框架进行管理

spl语句在函数注解中



```java
@Mapper
public interface UserMapper {
	@Insert("insert into users(name) value (#{name})")
	public int insertUser(String name);
	
}
```

测试类

```java
//@Autowired
private UserMapper userMapper;
@Test
public void InsertTest() {
	System.out.println(mapper.insertUser("TeamSea"));
}
```

运行结果

```
1
```

表示插入了一行数据

#### 项目原理分析

 ![](https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/16796466917691679646691005.png)

SpringBoot框架会Mapper接口注入一个接口实现类

这个类是由MyBatis框架提供的代理类`MapperProxy`

注意:这个类虽然有MyBatis框架提供,但是其使用了JDK中的代理工具类`Proxy`来实现

#### 验证是否使用了连接池

在HiKarCP中的`getConnection`方法上打一个断点,在进行debug,观察是否在这个断点中进行了停留

![](https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/16796482077581679648207045.png)

在HiKarCP中进行了停留,即应用和整合了HiKarCP连接池

#### 针对业务对SQL语句进行进阶管理

在之前,我们的SQL语句是写在Mapper接口类中的,好处是,对对Mapper方法和SQL清晰

但是这种方法不适用于复杂SQL语句和动态SQL语句(MyBatis框架特性之一)所以,在执行业务管理中,会将SQL语句写在`xml`文件中

创建一个mapper的xml文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.teamsea.mapper.UserMapper">
	
	<insert id="insertUsers">
		insert into users(name) values
		<foreach item="name" index="index" collection="names"
		separator=","
		>
		(#{name})
		</foreach>
	</insert>
	
</mapper>
```

将这个mapper文件交给SpringBoot框架进行管理

在`application.properties`文件中进行配置

```properties
#加载Mapper文件位置
#*表示所有的配置文件
mybatis.mapper-locations=classpath:mybatis/*.xml
```

#### 对SQL数据库加强操作性,健壮性,和提升性能

这里我们写一个多个用户删除的操作

```xml
<delete id="deleteUsers">
	delete from users where name in
	<foreach item="name" index="index" collection="names"
		open="(" close=")" separator="," 
	>
		#{name}
	</foreach>
</delete>
```

对应的接口类方法

```java
public int deleteUsers(@Param("names") String... names);
```

当我们不传递参数时,报错

```java
mapper.deleteUsers();// 这个语句会报错
```

我么可以针对这种情况进行优化

1. 健壮性优化

   ```xml
   <!--  对健壮性进行优化 -->
   	<delete id="deleteUsers">
   		delete from users
   		<!-- 当if 判断(传递的值不为空,而且有值时) 为真, 则执行删除语句-->
   		<if test="names!=null and names.length > 0">
   			where name in
   			<foreach item="name" index="index" collection="names"
   				open="(" close=")" separator=","
   			>
   			#{name}
   			</foreach>
   		</if>
   		<!-- 但是会出现一个问题, 当我们判断的值为假时, 没有where语句约束
   			sql语句会变成:
   			delete from users
   			会将表内的所有数据删除
   			所以我们要进行在判断
   		-->
   		<!-- 当为假时我们可以使用where语句进行约束,取消执行delete语句 -->
   		<if test="names=null and names.length = 0">
   			where 1=2<!-- where 假值 来取消执行sql语句 -->
   		</if>
   	</delete>
   ```

   当然使用这个`<if>`可以实现业务和需求,但是看起十分的冗杂

   使用`<choose>`进行优化

   ```xml
   <!--  对健壮性进行优化 -->
   	<delete id="deleteUsers">
   		delete from users
   		<!-- 使用choose进行优化 -->
   		<choose>
   			<when test="names!=null and names.length > 0">
   				where name in
   				<foreach item="name" index="index" collection="names"
   					open="(" close=")" separator=","
   				>
   					#{name}
   				</foreach>
   			</when>
   			
   			<otherwise>
   				where 1=2
   			</otherwise>
   		</choose> 
   ```

   

2. 对性能进行优化

   优化的sql的in的使用,

   in语句对于少量的值运行起来性能可行,但是处理大量的数据会有性能问题

   使用or语句来提升性能

   ```xml
   <delete id="deleteUsers">
   		delete from users
   			<where>
   				<choose>
   					<when test="names != null and names.length >0">
   						<foreach collection="names" separator="or" item="name">
   							name = #{name}
   						</foreach>
   					</when>
   					<otherwise>
   						1 = 2
   					</otherwise>	
   				</choose>
   			</where>
   	</delete>
   ```

## 2 构建业务层(Service)接口及实现类

使用Service来完成一个具体的业务.在实际开发中,一个业务要使用多个sql语句,并且其中有一定的逻辑关系.所以需要一个Service业务层来处理具体的事务

一个具体的Service通过接口来耦合,耦合到具体的Service实现类,之后通过Service实现类进行业务实现

![](https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/1679973252030image32.png)

Service接口

 ```java
 package com.teamsea.service;
 
 public interface UserService {
 	/**
 	 * 获取所有的User(id,name)属性
 	 */
 	public List<User> findUsers();
 
 }
 
 ```

Service 实现类

```java
package com.teamsea.service.impl;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper mapper;
	
	@Override
	public List<User> findUsers() {
		// 获取运行时间
		long start = System.currentTimeMillis();
		List<User> list = mapper.findUsers();
		long end = System.currentTimeMillis();
		System.out.println(end-start + "ms");
		return list;
	}

}

```

`@Service`注解解释:

![](https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/16799798710271679979870378.png)

可以说这是一个特殊的`@Component`注解,也是将这个类作为bean组件交给Spring框架管理,但是这个特殊的注解用来标注Service业务层

运行结果:

```tex
1041ms
[User [id=9, name=TeamSea], User [id=10, name=QC]]
```

结果分析:

因为在程序刚刚开始运行的时候,数据库连接池要去创建连接对象,创建连接对象是一件十分耗时的步骤

当程序,服务器创建连接对象,完成后,程序运行时间会大大缩短

![image-20230328131651608](C:\Users\lknlk\AppData\Roaming\Typora\typora-user-images\image-20230328131651608.png)

获取两次所用的时间完全不同少了约200倍

## 3 Spring Boot 整合Spring MVC应用

mvc 具体详见SpringMVC解释

> MVC（Model–view–controller）是软件工程中的一种软件架构模式，基于此模式把软件系统分为三个基本部分：模型（Model）、视图（View）和控制器（Controller）。目的是通过这样的设计使程序结构更加简洁、直观，降低问题的复杂度。其中各个组成部分的职责为：
>
> - 视图（View） - UI设计人员进行图形界面设计，负责实现与用户交互。
> - 控制器（Controller）- 负责获取请求，处理请求，响应结果。
> - 模型（Model） - 实现业务逻辑，数据逻辑实现。
>
> 我们在软件设计时，通常要遵循一定的设计原则。MVC架构模式的设计中，首先基于单一职责原则(SRP-Single responsibility principle)让每个对象各司其职，各尽所能。然后再基于“高内聚，低耦合”的设计思想实现相关层对象之间的交互。这样可以更好提高程序的可维护性和可扩展性。

![](https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/1679984521995image2.png)

Spring MVC 运行过程:

![](https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/1679987516995image1.png)

1. 客户端进行请求,通过过滤器进入前段控制器

2. 前端控制器:负责转发请求，接受用户的请求，申请处理后，将响应返回给客户

   前端控制器将要访问的url给HandlerMapping(处理器映射器)

3. HandlerMapping(处理器映射器):记录所有的Handler(处理器Controller),并且对其中的地址进行了映射关系

   根据前端映射器传递的url将对应的Handler(处理器Controller)的信息交给前端控制器

4. 前端控制器根据获取的信息调用对应的Handler(处理器Controller)

   调用前会先通过Interceptors(拦截器,实现请求响应的共性处理)

5. Handler(处理器Controller)将处理好的数据传递给前端控制器

6. 如果Handler(处理器Controller)响应的是一个页面,则会将这个页面在ViewResolver(视图解析器,解析对应的视图关系(前缀+viewname+后缀))进行解析

   如果不是,则不需要进行解析

7. 最后将数据或者页面传递给客户端

### 3.2 初始配置

添加依赖:

1. 添加spring web依赖

   这个依赖提供了Spring MVC 核心的API 同时嵌入一个Tomcat

2. 添加Thymeleaf依赖

   <img src="https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/16800670231301680067022262.png" style="zoom:67%;" />

#### 3.2.1 配置Spring MVC核心对象

```properties
#配置 Spring MVC 核心对象
spring.thymeleaf.prefix=classpath:/templates/pages/
spring.thymeleaf.suffix=.html
```

当然,Spring Boot 框架会将进行默认配置

前缀:prefix=classpath:/templates/

后缀:suffix=.html

### 3.3 Spring MVC 入门实践

编写一个Controller

```java
/**
 *  使用注解@Controller 声明这个类是 Controller
 *  
 *  使用注解@RequestMapping 表示这个类访问地址的前缀
 *  
 *  在方法的注解@@RequestMapping 表示 
 * */

@Controller

@RequestMapping("/users/")
public class UserController {

	@RequestMapping("userlist")
	public String UsersController() {
		return "userlist";
	}
}
```

# 联系

练习一:  将数据库中的商品数据查询出来更新到页面上。

练习二：基于ID删商品库中的商品信息。

练习三：将页面用户输入的商品信息写入到数据库。

